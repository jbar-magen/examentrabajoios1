//
//  AppDelegate.h
//  ExamenIOS1
//
//  Created by Yonathan Bar-Magen Numhauser on 05/12/15.
//  Copyright © 2015 Yonathan Bar-Magen Numhauser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Quickblox/Quickblox.h>
#import "Usuario.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

//Objeto USUARIOS
@property (strong, nonatomic) Usuario *usuario;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

