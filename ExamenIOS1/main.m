//
//  main.m
//  ExamenIOS1
//
//  Created by Yonathan Bar-Magen Numhauser on 05/12/15.
//  Copyright © 2015 Yonathan Bar-Magen Numhauser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
