//
//  CeldaColeccion.m
//  ExamenIOS1
//
//  Created by Yonathan Bar-Magen Numhauser on 06/12/15.
//  Copyright © 2015 Yonathan Bar-Magen Numhauser. All rights reserved.
//

#import "CeldaColeccion.h"
#import <Quickblox/Quickblox.h>

@implementation CeldaColeccion

//Metodo que fija la imagen de la celda teniendo como codigo de entrada el CID, el Content ID del QuickBlox.
-(void)setImagenUsandoContentID:(int)cid{
    [QBRequest downloadFileWithID:cid successBlock:^(QBResponse *response, NSData *fileData) {
        [imagenCelda setImage:[UIImage imageWithData:fileData]];
    } statusBlock:^(QBRequest *request, QBRequestStatus *status) {
        
    } errorBlock:^(QBResponse *response) {
        NSLog(@"error: %@", response.error);
    }];
}

@end
