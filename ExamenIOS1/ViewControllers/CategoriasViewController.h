//
//  CategoriasViewController.h
//  ExamenIOS1
//
//  Created by Yonathan Bar-Magen Numhauser on 06/12/15.
//  Copyright © 2015 Yonathan Bar-Magen Numhauser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriasViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UICollectionView *collection;
}
@end
