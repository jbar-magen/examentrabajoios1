//
//  ViewController.m
//  ExamenIOS1
//
//  Created by Yonathan Bar-Magen Numhauser on 05/12/15.
//  Copyright © 2015 Yonathan Bar-Magen Numhauser. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

//Metodo que devuelve la referencia al objeto unico AppDelegate de la aplicacion.
- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
